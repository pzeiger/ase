"""Langevin dynamics class."""

import numpy as np

from ase.md.md import MolecularDynamics
from ase.md.logger import MDLogger
from ase.parallel import world, DummyMPI, parprint
from ase.md.verlet import VelocityVerlet
from ase import units
import warnings
import pickle

from scipy.linalg import expm, sqrtm



class MDLoggerGLE(MDLogger):
    """
    """
    def __init__(self, dyn, atoms, logfile, header=True, stress=False,
                 peratom=False, mode="a"):
        super().__init__(dyn, atoms, logfile, header=False, 
                 stress=stress, peratom=peratom, mode=mode)
       
        # remove '\n' from end of format string
        self.fmt = self.fmt.rstrip()
        global_natoms = atoms.get_global_number_of_atoms()

        if self.peratom:
            self.hdr += "%16s" % "Hconserved[eV]"
            self.hdr += "%16s" % "dK[eV]"
            self.fmt += "%16.4f"
            self.fmt += "%16.4f"
        else:
            self.hdr += "%16s" % "Hconserved[eV]"
            self.hdr += "%16s" % "dK[eV]"
            
            # Choose a sensible number of decimals
            if global_natoms <= 100:
                digits = 4
            elif global_natoms <= 1000:
                digits = 3
            elif global_natoms <= 10000:
                digits = 2
            else:
                digits = 1
            self.fmt += ("%%16.%df " % (digits,))
            self.fmt += ("%%16.%df " % (digits,))
        
        self.fmt += "\n"
        if header:
            self.logfile.write(self.hdr + "\n")
        
    def __call__(self):
        epot = self.atoms.get_potential_energy()
        ekin = self.atoms.get_kinetic_energy()
        global_natoms = self.atoms.get_global_number_of_atoms()
        temp = ekin / (1.5 * units.kB * global_natoms)
        if self.peratom:
            epot /= global_natoms
            ekin /= global_natoms
        if self.dyn is not None:
            t = self.dyn.get_time() / (1000 * units.fs)
            dat = (t,)
        else:
            dat = ()
        dat += (epot + ekin, epot, ekin, temp)
        if self.stress:
            dat += tuple(self.atoms.get_stress(include_ideal_gas=True) / units.GPa)
        if self.dyn is not None:
            Hconserved = self.dyn.get_Hconserved()
            if self.peratom:
                Hconserved /= global_natoms
            dK = self.dyn.get_dK()
            dat += (Hconserved, dK,)
        
        self.logfile.write(self.fmt % dat)
        self.logfile.flush()



def get_hotspot_thermostat_matrices(T_base=300.0, inv_gamma_base=1.0, T_max=1000.0, 
                                    inv_gamma_peak=0.1, f_peak=20.0, delta_f=1.0):
    """
    Inputs:
    -------
    T_base                   baseline thermostat temperature in Kelvin
    iinv_gamma_base          in ps; 1/gamma_base
    T_max                    T_max in K
    inv_gamma_peak           1/gamma_hotspot in ps
    omega_peak               hotspot frequency in THz = ps^-1
    delta_omega              hotspot width in THz = ps^-1 
    
    Outputs:
    --------
    A                        drift_matrix in inverse ase time units
    C                        static_covariance_matrix in units of K
    
    Based on:
    R. Dettori, M. Ceriotti, J. Hunger, C. Melis, L. Colombo, and D. Donadio, J. Chem. Theory Comput. 13, 1284 (2017).
    """
    gb = 1. / inv_gamma_base
    g0 = 1. / inv_gamma_peak
    w0 = 2. * np.pi * f_peak
    dw = 2. * np.pi * delta_f

    T0 = T_max - (gb * (T_base - T_max) * (2.*np.pi*dw*(dw+gb) + g0 * w0)) / (dw * g0 * w0)
    
    A = np.array([[                            gb,  np.sqrt(g0 * w0 / (2.*np.pi)),         0.0],
                  [-np.sqrt(g0 * w0 / (2.*np.pi)),                             dw,          w0],
                  [                            0.,                            -w0,         0.0]])
    
    # Convert from picoseconds to ase units
    A = A  * 1 / (1e3 * units.fs)

    # The C matrix is obtained from solving the following expression:
    # A.C + C.A^T = D
    # We have carried this out analytically for the matrix D given in Dettori et al.. The result is given below.
    denom = (dw + gb) * (g0 * w0 + 2. * np.pi * dw * gb) + 2. * np.pi *dw * w0 * w0
    tmp = dw * np.sqrt(g0 * w0 * 2. * np.pi)
    
    C = np.array([[ dw * g0 * w0 * (T0 - T_base) / denom + T_base,                gb * tmp *(T0 - T_base) / denom,                   tmp * (-T0 + T_base) * w0 / denom],
                  [          gb * tmp * (T0 - T_base) / denom,   T0 + (g0 * w0 * gb * (-T0 + T_base)) / denom,                                              0.],
                  [         tmp * (-T0 + T_base) * w0 / denom,                                         0.,  T0 + (g0 * w0 *(dw + gb) * (-T0 + T_base)) / denom]])
    
    return A, C


def element_from_gen(generator):
    """
    """
    try:
        return next(generator)
    except StopIteration:
        return None



def get_unit_conversion_matrix(unit_string, matrix_id='A'):
    """
    """
    if matrix_id not in ('A', 'C'):
        raise ValueError('matrix_id must be either \'A\' or \'C\', currently: % s' % matrix_id)
    if matrix_id == 'A':
        if unit_string == 'femtoseconds^-1':
            conversion = 1/units.fs
        elif unit_string == 'picoseconds^-1':
            conversion = 1/(units.fs*1000.)
        elif unit_string == 'seconds^-1':
            conversion = 1/(units.second)
        else:
            raise NotImplementedError('Unit not implemented: %s' % unit_string)
    if matrix_id == 'C':
        if unit_string == 'eV':
            conversion = 1./units.kB
        elif unit_string == 'K':
            conversion = 1.
        elif unit_string == 'atomic energy units':
            conversion = units.Hartree/units.kB
        else:
            raise NotImplementedError('Unit not implemented: %s' % unit_string)
    return conversion


def process_matrix_file(matfile, matrix_id='A'):
    """ Reads matrices generted from
        https://gle4md.org/index.html?page=matrix

        matrix_id can be either 'A' or 'C'
    """
    if matrix_id not in ('A', 'C'):
        raise ValueError('matrix_id must be either \'A\' or \'C\', currently: % s' % matrix_id)
    
    lines = (line.strip() for line in open(matfile, 'r'))
    line = next(lines)
    
    mat = []
    while line is not None:
        if '# %s MATRIX' % matrix_id in line:
            unit_conversion = get_unit_conversion_matrix(line.split('(')[1].strip(')'),
                                                         matrix_id)
            line = element_from_gen(lines)
            while line is not None:
                if line == '':
                    break
                if line[0] == '#':
                    break
                mat.append(line)
                line = element_from_gen(lines)
        line = element_from_gen(lines)
    mat = np.loadtxt(mat) * unit_conversion
    return mat


class GLE(MolecularDynamics):
    """Generalized Langevin Equation molecular dynamics."""

    # Helps Asap doing the right thing.  Increment when changing stuff:
    _gle_version = 2
    
    def __init__(self, atoms, timestep, drift_matrix, temperature=300,
                 static_covariance_matrix=None, momenta=None,
                 fixcom=True, fixcommom=False, *,
                 trajectory=None, logfile=None,
                 loginterval=1, communicator=world, rng=None,
                 append_trajectory=False, debug=False):
        """
        Parameters:
        
        atoms: Atoms object
            The list of atoms.
        
        timestep: float
            The time step in ASE time units.
        
        temperature: float
            The desired temperature, in Kelvin. Only used if 
        
        drift_matrix: matrix
            The desired drift matrix (A-matrix). Can be either 
            a filename or a numpy matrix.
        
        static_covariance_matrix: matrix
            If none 

        momenta: matrix
            If None, momenta will be initialized from random gaussian numbers
            scaled by the C matrix. If provided, the momentum degrees of freedom
            of the thermostat will be initialized to the given matrix.
        
        fixcom: bool (optional)
            If True, the position of the center of mass is
            kept unperturbed.  Default: True.
        
        fixcommom: bool (optional)
            If True, the momentum of the center of mass is
            kept unperturbed.  Default: False.
        
        
        rng: RNG object (optional)
            Random number generator, by default numpy.random.  Must have a
             method matching the signature of
            numpy.random..

        logfile: file object or str (optional)
            If *logfile* is a string, a file with that name will be opened.
            Use '-' for stdout.

        trajectory: Trajectory object or str (optional)
            Attach trajectory object.  If *trajectory* is a string a
            Trajectory will be constructed.  Use *None* (the default) for no
            trajectory.

        communicator: MPI communicator (optional)
            Communicator used to distribute random numbers to all tasks.
            Default: ase.parallel.world. Set to None to disable communication.

        append_trajectory: bool (optional)
            Defaults to False, which causes the trajectory file to be
            overwritten each time the dynamics is restarted from scratch.
            If True, the new structures are appended to the trajectory
            file instead.
        
        
        Based on:
        Ceriotti, Bussi, Parrinello, J. Chem. Theory Comput. 6, 1170-80 (2010)

        """
        if atoms.constraints:
            raise NotImplementedError('GLE thermostatting does not support constraints at the moment.')
        self.debug = debug
        self.temp = temperature
        self.fixcom = fixcom
        self.fixcommom = fixcommom
        if communicator is None:
            communicator = DummyMPI()
        self.communicator = communicator
        if rng is None:
            self.rng = np.random.default_rng()
        else:
            self.rng = rng
        
        self.set_drift_matrix(drift_matrix)       
        self.set_static_covariance_matrix(static_covariance_matrix)
        
        assert self.A.shape == self.C.shape, 'Error: drift matrix and covariance matrix do not have the same size'
        
        self.vv_integrator = VelocityVerlet(atoms, timestep)
        
        self.dK = 0.0
        self.Hconserved = atoms.get_total_energy()
        
        MolecularDynamics.__init__(self, atoms, timestep, trajectory,
                                   logfile, loginterval,
                                   append_trajectory=append_trajectory)
        
        self._init_momenta(momenta=momenta)
        self.updatevars()

    
    def _init_momenta(self, momenta=None):
        if momenta is None:
            g = self.rng.standard_normal(size=(self.ns+1, len(self.atoms)*3))
            rootC = sqrtm(self.C * units.kB)
            self.s = np.matmul(rootC, g)
        else:
            self.s = momenta
    
    def todict(self):
        d = MolecularDynamics.todict(self)
        d.update({'temperature':   self.temp ,
                  'fixcm':         self.fixcom,
                  'A':             self.A,
                  'C':             self.C,
                  'dK':            self.dK,
                  'Hconserved':    self.Hconserved,
                  })
        return d
    
    def set_temperature(self, temperature=None):
        self.temp = temperature
        self.updatevars()
    
    def set_timestep(self, timestep):
        self.dt = timestep
        self.updatevars()
    
    def set_drift_matrix(self, drift_matrix):
        if isinstance(drift_matrix, str):
            self.A = process_matrix_file(drift_matrix, matrix_id='A')
        else:
            self.A = np.array(drift_matrix)
        assert len(self.A.shape) == 2
        assert self.A.shape[0] == self.A.shape[1]
        self.ns = self.A.shape[0]-1
        if self.debug:
            parprint('A: ', self.A)
    
    def set_static_covariance_matrix(self, static_covariance_matrix=None):
        if static_covariance_matrix is not None:
            self.noneq = True
            if isinstance(static_covariance_matrix, str):
                self.C = process_matrix_file(static_covariance_matrix, matrix_id='C')
            else:
                self.C = np.array(static_covariance_matrix)
        else:
            self.noneq = False
            self.C = self.temp * np.identity(self.ns+1)
        if self.debug:
            parprint('C: ', self.C)
    
    def get_Hconserved(self):
        return self.Hconserved
    
    def get_dK(self):
        return self.dK
    
    def updatevars(self):
        if not self.noneq:
            self.set_static_covariance_matrix()
        
        self.T = expm(-self.dt*.5*self.A) 
        self.TT = np.transpose(self.T) 
        
        SST = units.kB * (self.C - np.matmul(self.T, np.matmul(self.C, self.TT)))
        self.S = sqrtm(SST)
        
        # it can happen, that S is a complex matrix. This would later lead to complex
        # momenta, which is unphysical. Thus we simply ignore the imaginary part and
        # raise a warning to inform the user.
        if self.S.dtype == 'complex128':
            warnings.warn('updatevars: encountered complex values in matrix '\
                          + 'decomposition S = np.sqrtm(SST). Discarding '\
                          + 'imaginary part... Check that matrix below is small.\n'\
                          + 'S.real*ST.real-SST in eV: \n'\
                          + np.array2string((SST-np.matmul(self.S.real, self.S.real.T)))
                          )
            parprint('\nupdatevars: encountered complex values in matrix '\
                    + 'decomposition S = np.sqrtm(SST). Discarding imaginary part... '\
                    + 'Check that matrix below is small.')
            parprint('SST-S.real*ST.real in eV:\n', 
                    (SST-np.matmul(self.S.real, self.S.real.T)))
            parprint('SST-S.real*ST.real in Hartree:\n', 
                    (SST-np.matmul(self.S.real, self.S.real.T))/units.Hartree)
            self.S = self.S.real
        self.ST = np.transpose(self.S)
        
        if self.debug:
            parprint('\nT: ', self.T)
            parprint('SST in eV:', SST)
            parprint('SST in Hartree:', SST/units.Hartree)
            parprint('S in sqrt(eV):', self.S)
            parprint('S in sqrt(Hartree):', self.S/np.sqrt(units.Hartree))
        
        self.sm = np.sqrt(self.atoms.get_masses()[:,np.newaxis])
   
    
    def save_glestate(self, fname_matrices='gle_state.npz', 
                      fname_rng_state='gle_rng_state.pkl'):
        """
        """
        np.savez(fname_matrices, s=self.s, A=self.A, C=self.C)
        if fname_rng_state is not None:
            if isinstance(self.rng, np.random.Generator):
                pickle.dump(self.rng.bit_generator, open(fname_rng_state, 'wb'))
            elif isinstance(self.rng, np.random.RandomState):
                pickle.dump(self.rng.get_state(), open(fname_rng_state, 'wb'))
            else:
                raise NotImplementedError('Don\'t know how to save state of' \
                                           + 'random number generator')
    

    def _free_particle_gle_halfstep(self):
        """
        """
        
        atoms = self.atoms
        natoms = len(self.atoms)
        
        self.p = atoms.get_momenta()
        self.dK += atoms.get_kinetic_energy()
       
        # import momenta into extended momentum array
        self.s[0,:] = (self.p / self.sm).flatten()
        
        # update momenta
        self.s = np.matmul(self.T, self.s) + np.matmul(self.S, self.rng.standard_normal(size=self.s.shape))
        
        # set momenta
        self.p = self.s[0,:].reshape((-1,3)) * self.sm
        atoms.set_momenta(self.p)
        
        # Compute conserved quantity
        self.dK -= atoms.get_kinetic_energy()
        self.Hconserved = atoms.get_total_energy() + self.dK

   
    def _vv_step(self, forces=None):
        """
        """
        parprint('vv step')
        atoms = self.atoms
        
        if forces is None:
            forces = atoms.get_forces()
        
        p = atoms.get_momenta()
        p += 0.5 * self.dt * forces
        masses = atoms.get_masses()[:, np.newaxis]
        r = atoms.get_positions()
        
        # if we have constraints then this will do the first part of the
        # RATTLE algorithm:
        atoms.set_positions(r + self.dt * p / masses)
        if atoms.constraints:
            p = (atoms.get_positions() - r) * masses / self.dt
        
        # We need to store the momenta on the atoms before calculating
        # the forces, as in a parallel Asap calculation atoms may
        # migrate during force calculations, and the momenta need to
        # migrate along with the atoms.
        atoms.set_momenta(p, apply_constraint=False)
        
        forces = atoms.get_forces(md=True)
        
        # Second part of RATTLE will be done here:
        atoms.set_momenta(atoms.get_momenta() + 0.5 * self.dt * forces)
    
    
    def step(self, forces=None):
        """
        """
        
        # first half-step of GLE thermostat
        self._free_particle_gle_halfstep()
        
        # VelocityVerlet integrator does not offer option to fix com
        if self.fixcom:
            old_com = self.atoms.get_center_of_mass()
        
        self.vv_integrator.step(forces)
#        self._vv_step(forces)
        
        if self.fixcom:
            self.atoms.set_center_of_mass(old_com)
        
        # second GLE halfstep
        self._free_particle_gle_halfstep()
        
        # subtract center of mass momentum
        if self.fixcommom:
            self.p = self.atoms.get_momenta()
            self.p -= self.atoms.get_center_of_mass_momentum() \
                      / self.atoms.get_global_number_of_atoms()
            self.atoms.set_momenta(self.p)
        
        
        return forces
